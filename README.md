This is a more heavier Neovim config than my other one.

You will need 'nodejs', 'npm', 'rg', 'packer.nvim'. 'lua', 'nerd-fonts-complete' and ofcourse Neovim itself

After that run:

git clone https://gitlab.com/thatoneguy3/simple-lua-neovim-config
Then backup your existing neovim config
Then move the files in 'simple-lua-neovim-config/nvim' folder and do them in '~/.config/nvim'
Then run 'PackerInstall' and you should be good to go/

Enjoy.
