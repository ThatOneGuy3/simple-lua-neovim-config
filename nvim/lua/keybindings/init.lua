vim.g.mapleader = ' '
local map = vim.api.nvim_set_keymap
map('n', '<C-h>', '<C-w>h', {noremap = true, silent = false})
map('n', '<C-l>', '<c-w>l', {noremap = true, silent = false})
map('n', '<C-j>', '<c-w>j', {noremap = true, silent = false})
map('n', '<C-k>', '<c-w>k', {noremap = true, silent = false})

map('n', 'f', ':NvimTreeToggle<CR>', {noremap = true, silent = false})
